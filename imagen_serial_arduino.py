#      80
#   --------
#           |         Matriz de pixeles de 1 byte
#           |         "Coordenadas polares"
#           | 2000    160.000 bytes
#           |
#           |
#
#   Comunicacion Serial: 1bit, no paridad
#   No hay "headers" ni nada, se envian los 160mil datos de una
#   + parametros de captura del ecografo
#
#   Lenna.jpg 512x512 = 262.144 pixeles de 1 byte (escala de grises)


import serial
from time import sleep
from PIL import Image

img = Image.open('Lenna.png').convert('L')
width, height = img.size

# img.show()
print img.size
print img.getpixel((0,511))

#port = "/dev/cu.wchusbserial1410"
port = "/dev/ttyUSB0"

try:    hc05 = serial.Serial(port, 9600)
except: print "Error en conexion serial"

print 'Puerto serial (' + port + ') abierto'
sleep(2)

#raw_input('Enviar Lenna?')

# for i in range(0, width):
#     for j in range(0, height):
#         ser.write(img.getpixel(i,j))

#for i in range(43,61):
#    ser.write(i)
hc05.write('123456789'); sleep(0.01)#-123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')
hc05.write('123456789'); sleep(0.01)
hc05.write('123456789'); sleep(0.01)
hc05.write('123456789'); sleep(0.01)
hc05.write('123456789'); sleep(0.01)
hc05.write('123456789'); sleep(0.01)
hc05.write('123456789'); sleep(0.01)
hc05.write('123456789'); sleep(0.01)
hc05.write('0'); sleep(0.01)
#hc05.write(0x30)

hc05.close()
