#include <SoftwareSerial.h>

#define LED 13
#define BOTON 2

SoftwareSerial mySerial(10, 11); // RX, TX

#define msg_length 20000
char c = 48;
long count = 0;

void setup() {
  pinMode(BOTON, INPUT_PULLUP);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH); delay(300); digitalWrite(LED_BUILTIN, LOW);
  Serial.begin(9600);
  mySerial.begin(9600);
}

void loop() {
  while (digitalRead(BOTON) == HIGH) {
  }
  digitalWrite(LED, HIGH);
  
  while (count < msg_length) {
    mySerial.write(c);
    count++;
    c++;
  }
  count = 0;

  digitalWrite(LED, LOW);
}

/*
void setup() {
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if (Serial.available()) {
    if (Serial.read() == '0') {
      digitalWrite(13, HIGH);
      delay(1000);
    }
  }
  digitalWrite(13, LOW);
}
*/
/*
#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // RX, TX

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
}

void loop() {
  if (Serial.available()) {
    mySerial.write(Serial.read());
  }
}
*/









